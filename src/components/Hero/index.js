import React, { useEffect, useState } from "react";
import HeroImg from "../../assets/img/Hero.png";
import './style.scss'
import { useDispatch, useSelector } from "react-redux";
import { setDataCard } from "../../config/redux/action";



function Index() {
  const { dataCard, page } = useSelector((state) => state.dataCard);
  console.log("state blogs global: ", dataCard);
  
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setDataCard());
  }, [ dispatch]);

  return (
    <div className="relative background-pink">
      <div className="float-right background-pink ">
        <img
          src={HeroImg}
          className="float-right origin-bottom -rotate-90 background-pink"
          style={{ width: 140, marginRight: -70, backgroundColor: "" }}
        />
      </div>

      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />

      <div className="flex items-center justify-center h-80" >
        <div
          className=" font-bold rounded-lg p-10 inline text-right  w-w-5/6 md:w-3/6  "
          style={{ marginTop: -200 }}
        >
          <div style={{ color: "blue" }} className="inline">
            Deffinition;&nbsp;
          </div>
          a practice or exercise to test or improve one's fitness for athletic
          competition, ability, or performance to exhaust (something, such as a
          mine) by working to devise, arrange, or achieve by resolving
          difficulties. Merriam-Webster.com Dictionary.
          <p
            className="mt-10 text-white"
            style={{ fontStyle: "italic", fontSize: 16 }}
          >
            -weekend team
          </p>
        </div>
      </div>

      <br />
      <br />
      <br />

      <h1
        className="text-white text-left md:text-center ml-5 "
        style={{ marginTop: -200, fontSize: 32, fontWeight: 800 }}
      >
        Testimonial
      </h1>
 
      <div className="two-colour-background">
        
        

      <div className="grid grid-cols-1 md:grid-cols-3 gap-2 mt-10 ">
        <div className="container mx-auto px-5">
          <div className="p-6 bg-gray-100 rounded-lg">
            <div className="mb-5"></div>

            <h3 className="text-lg font-bold mb-2">{dataCard[0]?.by}</h3>

            <p className="text-sm leading-6 text-gray-600">
              {dataCard[0]?.testimony}
            </p>
          </div>
        </div>
        <div className="container mx-auto px-5">
          <div className="p-6 bg-gray-100 rounded-lg">
            <div className="mb-5"></div>

            <h3 className="text-lg font-bold mb-2">{dataCard[2]?.by}</h3>

            <p className="text-sm leading-6 text-gray-600">
            {dataCard[2]?.testimony}
            </p>
          </div>
        </div>
        <div className="container mx-auto px-5">
          <div className="p-6 bg-gray-100 rounded-lg">
            <div className="mb-5"></div>

            <h3 className="text-lg font-bold mb-2">{dataCard[1]?.by}</h3>

            <p className="text-sm leading-6 text-gray-600">
            {dataCard[1]?.testimony}
            </p>
          </div>
        </div>
      </div>
      </div>

    </div>
  );
}

export default Index;
