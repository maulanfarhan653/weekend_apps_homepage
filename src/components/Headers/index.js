import React from "react";
import ImageFellas from "../../assets/img/Bitmap.png";

function Index() {
  return (
    <div>
      <div className="ml-8 pt-4 md:ml-60 pt-4 pb-4 ">
        <div className="flex gap-2">

        <img src={ImageFellas} width={33} />

        <div className="grid grid-rows-2 grid-flow-col">
        <p style={{ fontWeight: 400, fontSize:14, textAlign:'left'  }} alt="image"> Good Morning!</p>
        <p style={{ fontWeight: "bolder", fontSize:16, textAlign:'left' }} alt="image"> Fellas</p>
        </div>
        </div>
      </div>
    </div>
  );
}

export default Index;
