const initialStateHome = {
    dataCard: [],
    page : {
    currentPage: 1,
    totalPage : 1
}
}

const cardReducer =(state = initialStateHome,action) =>{
    if(action.type === 'UPDATE_DATA_CARD'){
        return {
            ...state,
            dataCard: action.payload
        }
    }

if(action.type === 'UPDATE_PAGE'){
return {
    ...state,
    page:action.payload
}
}

    return state;
}

export default cardReducer