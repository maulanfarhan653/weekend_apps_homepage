import axios from "axios";

export const setDataCard = (page) => (dispatch) => {
  axios
    .get(`https://wknd-take-home-challenge-api.herokuapp.com/testimonial`)
    .then((result) => {
      const responseAPI = result.data;
    //   console.log(responseAPI)

      dispatch({ type: "UPDATE_DATA_CARD", payload: responseAPI });
      dispatch({
        type: "UPDATE_PAGE",
         payload: {
           currentPage: responseAPI.current_page, 
        totalPage: Math.ceil(responseAPI.total_data / responseAPI.per_page)
      }
    })
    })
    .catch((err) => {
      console.log("error : ", err);
    });
};  
